import datetime
import multiprocessing
import time
import requests

# 谷歌浏览器中
# CSRF，ID在你要领取的直播奖励界面点进去进去F12再F5刷新看到网络中有一个链接(https://api.bilibili.com/x/activity/mission/single_task?csrf=xx&id=xx)可以看到
csrf = "37b194ef13f7a3d10721b64921939ec0"
id = ""
cookie = "buvid3=A808B958-D06B-5AA0-7098-7E1EB08CD08962314infoc; b_nut=1700619862; i-wanna-go-back=-1; b_ut=7; _uuid=A5110D484-3754-ADF8-B210E-610E465E83610262345infoc; enable_web_push=DISABLE; buvid4=6D3D9ADD-F717-1883-7497-E4CBFF20819563200-023112202-; rpdid=|(JYYJl|m~~)0J'u~||Yum))); DedeUserID=18069495; DedeUserID__ckMd5=59abccb126f39f36; header_theme_version=CLOSE; buvid_fp_plain=undefined; CURRENT_FNVAL=4048; LIVE_BUVID=AUTO9217008008685109; hit-dyn-v2=1; fingerprint=ba16c7aa01cd87cb2e0dc62681c24795; CURRENT_QUALITY=80; home_feed_column=5; browser_resolution=1912-958; share_source_origin=COPY; bsource=share_source_copy_link; bp_video_offset_18069495=882629952728465490; SESSDATA=8c2b8bdb%2C1720226631%2Cc5970%2A12CjCVjAavHPCXCXLt9pxRttGp81TdQTDkx7SOeeHNO_Co74WOKX18Z_MrTiFH2LdAGdQSVi1HZTM0a3h0QXY4QjRab2h2N25XRGJGSHdzWkE2VTZQUEJQMG9EUVlzSmhWdkVkM1Z5RDRVdnVxN0ZlTVFRUlhpNWNHZGFtVjFZRzQ5TTZqZjVtMmtBIIEC; bili_jct=37b194ef13f7a3d10721b64921939ec0; sid=6hbywl9l; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDQ5NTYxODQsImlhdCI6MTcwNDY5NjkyNCwicGx0IjotMX0.8Zdhnf6bFrN1Re5XjGI4nIASOhLNqxQ0_DTwiUU33yM; bili_ticket_expires=1704956124; innersign=0; b_lsid=A9C1CD82_18CE851E801; buvid_fp=ba16c7aa01cd87cb2e0dc62681c24795; PVID=2; x-bili-gaia-vtoken=2d4544a7d2854d9e9a7f28eba773deec"


# 奖励领取资格查询
def isCan():
    url = f"https://api.bilibili.com/x/activity_components/mission/info?csrf={csrf}&task_id={id}"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.69",
        "cookie": cookie,
    }
    res = requests.get(url, headers=headers).json()
    print(res)
    flag = False
    if res['code'] == 0:
        flag = res['data']['task_finished']
    if flag:
        return res['data']
    else:
        return 0


def receive(info):
    url = "https://api.bilibili.com/x/activity_components/mission/receive"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.76",
        "cookie": cookie,
    }
    data = {
        "csrf": csrf,
        "receive_from": "missionPage",
        "task_id": info['task_id'],
        "task_name": info['task_name'],
        "reward_name": info['reward_info']['award_name'],
        "activity_id": info['act_id'],
        "activity_name": info['act_name'],
    }
    res = requests.post(url, headers=headers, data=data).json()
    time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
    print(time_str, res)
    return res


if __name__ == '__main__':
    id = input('请输入兑换任务ID：')
    result = None
    while True:
        result = isCan()
        if result != 0:
            print("已经获取到资格")
            break
        time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        print(time_str + ":等待领取资格")
        time.sleep(0.5)
    now = datetime.datetime.now() + datetime.timedelta(days = 1)
    tomorrow = now.replace(hour=0, minute=0, second=0, microsecond=0)
    # while True:
    #     if datetime.datetime.now() >= tomorrow:
    #         break
    #     else:
    #         delta = tomorrow - datetime.datetime.now()
    #         time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
    #         remaining_hours, remaining_seconds = divmod(delta.total_seconds(), 3600)
    #         remaining_minutes, remaining_seconds = divmod(remaining_seconds, 60)
    #          # 输出剩余时间
    #         print("现在时间：" + time_str + "，倒计时：{:02d}:{:02d}:{:02d}".format(int(remaining_hours), int(remaining_minutes),
    #                                                                        int(remaining_seconds)))
    #         time.sleep(1)
    # 线程数
    pool = multiprocessing.Pool(10)
    while True:
        receiveResult = pool.apply_async(func=receive, args=(result,))
        if receiveResult.get()['code'] == 0 or receiveResult.get()['code'] == 75154 \
                or receiveResult.get()['code'] == 75086 or receiveResult.get()['code'] == 75255 \
                or receiveResult.get()['code'] == -101:
            break
    pool.close()
    pool.join()

import datetime
import multiprocessing
import time
import requests

# 谷歌浏览器中
# CSRF，ID在你要领取的直播奖励界面点进去进去F12再F5刷新看到网络中有一个链接(https://api.bilibili.com/x/activity/mission/single_task?csrf=xx&id=xx)可以看到
csrf = "ef7939908fb79c246d8cbd4a015f1073"
id = ""
cookie = "buvid3=B54418B5-DF01-1CD8-DC58-CC30ED55192229345infoc; b_nut=1691923729; i-wanna-go-back=-1; b_ut=7; _uuid=F5E3A1E4-6663-B610F-CA89-3A2F8F6AA710126928infoc; buvid4=8095C37E-A4C0-FA65-DBAD-28912624A1C730116-023081318-hUSwuB4vsUfMQ%2BkOok036g%3D%3D; CURRENT_FNVAL=4048; rpdid=|(umk)uuuRRR0J'uYmumkJ~RJ; buvid_fp_plain=undefined; DedeUserID=18069495; DedeUserID__ckMd5=59abccb126f39f36; CURRENT_QUALITY=80; header_theme_version=CLOSE; LIVE_BUVID=AUTO9916930551642672; enable_web_push=DISABLE; home_feed_column=5; FEED_LIVE_VERSION=V8; browser_resolution=1912-958; PVID=1; bp_video_offset_18069495=912125279891095593; fingerprint=10141d752223f53b39b54d74d6dac4c6; b_lsid=AE8281410_18EB3A17042; SESSDATA=06055cb8%2C1727962687%2Ce8b4b%2A42CjBr5E3aOajPpsX77v2LjnmGFMA3Opb89b8vc9Ia-clOhwwLlI0IVwp0T98Icx4uCE8SVjltMnE0WmgtMWdZdHE0NnZNNy0xVHhJNjRHUmE0WU9WV0J5MlhHRjdvUGlCUXF6bXFIUU1DcEVCbkw4M3VoRk82NE5vSV9SUGs0M1ZsbGUzZy1HbW5nIIEC; bili_jct=ef7939908fb79c246d8cbd4a015f1073; sid=6bfvv59l; bili_ticket=eyJhbGciOiJIUzI1NiIsImtpZCI6InMwMyIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MTI2Njk4OTIsImlhdCI6MTcxMjQxMDYzMiwicGx0IjotMX0.QEENkxG6qhlK89sR-nv-nTFzrUh5_nmGf7tW2fVsAOE; bili_ticket_expires=1712669832; buvid_fp=10141d752223f53b39b54d74d6dac4c6"


# 奖励领取资格查询
def isCan():
    url = f"https://api.bilibili.com/x/activity/mission/single_task?csrf={csrf}&id={id}"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.69",
        "cookie": cookie,
    }
    res = requests.get(url, headers=headers).json()
    print(res)
    flag = 0
    if res['code'] == 0:
        flag = res['data']['task_info']['receive_id']
    if flag > 0:
        return res['data']['task_info']
    else:
        return 0


def receive(info):
    url = "https://api.bilibili.com/x/activity/mission/task/reward/receive"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.76",
        "cookie": cookie,
    }
    data = {
        "csrf": csrf,
        "receive_from": "missionLandingPage",
        "receive_id": info['receive_id'],
        "group_id": info['group_list'][0]['group_id'],
        "task_id": info['id'],
        "act_id": info['act_id'],
        "task_name": info['task_name'],
        "reward_name": info['reward_info']['reward_name'],
        "act_name": '崩坏星穹铁道1.4视频侧',
    }
    try:
	    res = requests.post(url, headers=headers, data=data).json()
	    time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
	    print(time_str, res)
	    return res
    except:
        time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
	    print(time_str, "领取接口请求失败，正在重新请求...")
    


if __name__ == '__main__':
    id = input('请输入兑换任务ID：')
    result = None
    while True:
        result = isCan()
        if result != 0:
            print("已经获取到资格")
            break
        time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
        print(time_str + ":等待领取资格")
        time.sleep(0.5)
    now = datetime.datetime.now()
    tomorrow = now.replace(hour=23, minute=59, second=59, microsecond=0)
    while True:
        if datetime.datetime.now() >= tomorrow:
            break
        else:
            delta = tomorrow - datetime.datetime.now()
            time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            remaining_hours, remaining_seconds = divmod(delta.total_seconds(), 3600)
            remaining_minutes, remaining_seconds = divmod(remaining_seconds, 60)
             # 输出剩余时间
            print("现在时间：" + time_str + "，倒计时：{:02d}:{:02d}:{:02d}".format(int(remaining_hours), int(remaining_minutes),
                                                                           int(remaining_seconds)))
            time.sleep(1)
			
    # 线程数
    pool = multiprocessing.Pool(1)
    while True:
        receiveResult = pool.apply_async(func=receive, args=(result,))
        if receiveResult.get() is None:
           continue
        if receiveResult.get()['code'] == 0 or receiveResult.get()['code'] == 75154\
                or receiveResult.get()['code'] == 75086 or receiveResult.get()['code'] == 75255:
            break
    pool.close()
    pool.join()

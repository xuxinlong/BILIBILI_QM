import datetime
import multiprocessing
import time
import requests

# cookie
cookie = "dy_did=9c63071bff15abb40755dd2400031601; dy_did=9c63071bff15abb40755dd2400031601; acf_did=9c63071bff15abb40755dd2400031601; dy_teen_mode=%7B%22uid%22%3A%229990275%22%2C%22status%22%3A0%2C%22birthday%22%3A%22%22%2C%22password%22%3A%22%22%7D; acf_uid=9990275; acf_username=qq_mHoiCNts; acf_nickname=%E4%BB%B0%E6%9C%9B%E6%98%9F%E7%A9%BA5477; acf_own_room=1; acf_groupid=1; acf_phonestatus=1; acf_ct=0; acf_ltkid=65338240; acf_biz=1; acf_auth=809cC1ikSJ0RcrvoPHUEEjIO6TBgbbOgYTjxtzeFI8z7K%2BU3jGk5kol6Nq6GqFyuRayOZp1eAG1FGKV9UxfivFWgzXepchtAdpQ0bF%2FVi0JwGY5HxNgv4Fo; dy_auth=8c65HzLf%2Fxm3wTMYcU9l%2FjFOE8cj5UYPaR0sPC%2FbHrD1k8KyK5h4%2FM4lM%2BZcMJ%2Bch5%2FPXl2Cfls6xera1pFviS%2Bme%2F8XHxmaeXRPxStyFdfgNFlE1lQZMKk; wan_auth37wan=c205ff761a3bWIq%2FDHTi5r1%2FCgilXFV1QcFpUCmwW0Jj4rh%2FM%2BlN7nwCSxoXrqj%2FMOOLa2%2FG5l98BDikivUyZyDAx2kkgUIy6zL3Iz9%2BlRbthB0j; acf_stk=c65e05db4c07bd18; acf_avatar=//apic.douyucdn.cn/upload/avatar/009/99/02/75_avatar_; Hm_lvt_e99aee90ec1b2106afe7ec3b199020a7=1693612793,1693969069,1694132120,1694161614; PHPSESSID=2iar5f0qa400h7lh9mbhnm8vb2; acf_ccn=1f6365278c4f3fce7c5a23c941ff6d20; Hm_lpvt_e99aee90ec1b2106afe7ec3b199020a7=1694161618"


def receive(taskId):
    url = "https://www.douyu.com/japi/carnival/nc/web/roomTask/getPrize"
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36 Edg/116.0.1938.69",
        "cookie": cookie,
    }
    data = {
        "taskId": taskId
    }
    res = requests.post(url, headers=headers, data=data).json()
    print(res)
    return res


if __name__ == '__main__':
    taskId = input('请输入兑换任务ID：')
    now = datetime.datetime.now()
    tomorrow = now.replace(day=now.day, hour=18, minute=0, second=0, microsecond=0)
    while True:
        if datetime.datetime.now() >= tomorrow:
            break
        else:
            delta = tomorrow - datetime.datetime.now()
            time_str = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time()))
            remaining_hours, remaining_seconds = divmod(delta.total_seconds(), 3600)
            remaining_minutes, remaining_seconds = divmod(remaining_seconds, 60)
            # 输出剩余时间
            print("现在时间：" + time_str + "，倒计时：{:02d}:{:02d}:{:02d}".format(int(remaining_hours), int(remaining_minutes),
                                                                          int(remaining_seconds)))
            time.sleep(1)
    # 线程数
    pool = multiprocessing.Pool(10)
    while True:
        receiveResult = pool.apply_async(func=receive, args=(taskId,))
        if receiveResult.get()['error'] == 0:
            break
    pool.close()
    pool.join()
